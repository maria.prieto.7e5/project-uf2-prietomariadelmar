package model

data class Film(val title: String, val director: String, val mainActor: String, val genere: String, val length: Int) {

}
