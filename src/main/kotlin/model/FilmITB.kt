package model

data class FilmITB(
    val userList: MutableList<User> = mutableListOf(),
    val filmList: MutableList<Film> = mutableListOf(),
) {
    /**
     * Añade un usuario a la lista de usuarios
     */
    /**
     ** @param newUser pedimos el nuevo usuario
     */
    fun addUser(newUser: User) {
        userList.add(newUser)
    }
    /**
     * Actualizamos el usuario
     */
    /**
     ** @param currentUsername el usuario actual
     * @param newUsername pedimos el nuevo usuario
     */

    fun updateUser(currentUsername: User, newUsername: String) {
        println("Enter your current user")
        for (user in userList) {
            if (user == currentUsername) {
                user.username = newUsername
                break
            }
        }
    }
    /**
     * Eliminamos el usuario
     */
    /**
     ** @param username introducimos el usuario
     */

    fun deleteUser(username: User) {
        for (user in userList) {
            if (user == username) {
                userList.remove(user)
                break
            }
        }
    }

    fun changeUser(user: User) {
    }

    fun addFilm(newFilm: Film) {
        filmList.add(newFilm)
    }
    /**
     * Eliminamos la pelicula
     */
    /**
     ** @param film introducimos la pelicula
     */

    fun deleteFilms(film: Film) {
        for (movie in filmList) {
            if (movie.title == film) {
                filmList.remove(movie)
                break
            }
        }
    }

    fun watchFilm(film: Film) {
        //  TODO
        //user.add(film)
    }

    fun favoriteFilm(film: Film){
        //TODO
       // user.addFavotiteFilm(film)
    }


}