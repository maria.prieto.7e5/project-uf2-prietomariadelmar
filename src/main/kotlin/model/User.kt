package model

import model.Film

data class User(var username: String) {

    val watchedFilms get() = mutableListOf<Film>()
    val favoritesFilms get() = mutableListOf<Film>()

    fun addWatchFilm(film: Film){

        watchedFilms.add(film)
    }

    fun addFavoriteFilm(film: Film){

        favoritesFilms.add(film)
    }
}