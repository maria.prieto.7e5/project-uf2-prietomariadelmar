package ui
import model.Film
import model.FilmITB
import model.User

import java.util.*

class UserUI(val sc: Scanner, val appState : AppState){
    val filmITB=FilmITB()
    /**
     * Inicia el programa
     */
    fun start() {
        do {
            showMenu()
            var option = sc.nextInt()
            executeOption(option)
        } while (option != 0)
    }
    /**
     * Imprime el menu de opciones de usuarios
     */
    fun showMenu(){
        println("  Users:\n" +
                " 1: Add user\n" +
                " 2: Show my user\n" +
                " 3: View users\n" +
                " 4: Update user\n" +
                " 5: Delete user\n" +
                " 6: Change User\n" +
                " 7: Show statistics\n" +
                " 0: Return to main menu")
    }
    /**
     * Imprime el menu de opciones de pelicula
     */
    fun executeOption(option:Int){
        when (option) {
            1-> filmITB.addUser(User(sc.nextLine()))
            2->filmITB.
            3->
            4->filmITB.updateUser(User(sc.nextLine()),sc.nextLine())
            5->filmITB.deleteUser(User(sc.nextLine()))
            6->filmITB.changeUser()
            7->
            0->
        }
    }
    fun setCurrentUser() {
        println("Select username")
        val newUser = sc.next()
        appState.updateCurrentUser(newUser)
    }
}
