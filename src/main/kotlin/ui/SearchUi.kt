package ui

import model.Film
import model.FilmITB
import java.util.*

data class SearchUi(val sc: Scanner, val filmITB: FilmITB) {
    /**
     * Inicia el programa
     */
    fun start() {
        do {
            showMenu()
            var option=sc.nextInt()
            executeOption(option)
        }while (option!= 0)

    }
    /**
     * Imprime el menu de opciones de pelicula
     */
    fun showMenu(){
        println("Search methods:\n" +
                "1: By title\n" +
                "2: By director\n" +
                "3: By main actor\n" +
                "4: By genere\n" +
                "5: By length\n" +
                "6: Not watched\n" +
                "7: Recomended\n" +
                "0: Return to main menu:")

    }
    fun executeOption(option: Int){
        when (option) {
            1 ->searchByTitle(sc.nextLine())
            2 ->searchByDirector(sc.nextLine())
            3 ->searchByMainActor(sc.nextLine())
            4->searchByGenere(sc.nextLine())
            5->searchByLength(sc.nextInt())
            6->notWatched()
            7->recomended()
        }
    }
    /**
     * Busca una pelicula por titulo dentro del listado de peliculas
     */
    /**
     ** @param title introducimos el titulo de la pelicula que deseamos buscar
     */
    fun searchByTitle(title: String) = filmITB.filmList.filter{ x -> x.title == title}
    /**
     * Busca una pelicula por su director dentro del listado de peliculas
     */
    /**
     ** @param director introducimos el director de la pelicula que deseamos buscar
     */
    fun searchByDirector(director: String)=filmITB.filmList.filter{ x -> x.director == director}
    /**
     * Busca una pelicula por su actor principal dentro del listado de peliculas
     */
    /**
     ** @param mainActor introducimos el actor principal de la pelicula que deseamos buscar
     */
    fun searchByMainActor (mainActor: String)=filmITB.filmList.filter{ x -> x.mainActor ==mainActor}
    /**
     * Busca una pelicula por su genero dentro del listado de peliculas
     */
    /**
     ** @param genere introducimos el genero de la pelicula que deseamos buscar
     */
    fun searchByGenere(genere: String)=filmITB.filmList.filter{ x -> x.genere ==genere}
    /**
     * Busca una pelicula por su duracion dentro del listado de peliculas
     */
    /**
     ** @param length introducimos la duracion de la pelicula que deseamos buscar
     */
    fun searchByLength(length : Int)=filmITB.filmList.filter{ x -> x.length ==length}
    fun notWatched(){


    }
    fun recomended(){

    }



}