package ui

import model.FilmITB
import java.util.*

fun main() {
    val sc=Scanner(System.`in`)
    val ui = UI(sc)
    ui.start()

}

class UI(val sc :Scanner){
    val appState=AppState(currentUser,filmITB, sc)
   val userUI=UserUI(sc,appState)
    val filmITB=FilmITB()
    val filmUI=FilmUI(filmITB,sc)
    val searchUI=SearchUi(sc,filmITB)

    /**
     * Inicia el programa
     */
    fun start() {
        //TODO:usuario, seleccionar usuario
        do {
            showMenu()
            var option=sc.nextInt()
            executeOption(option)
        }while (option!= 0)

        }

    /**
     * Imprime el menu de opciones de usuarios
     */
     fun showMenu(){
         println( "Welcome FilmItb:\n" +
                 "1: User\n" +
                 "2: Films\n" +
                 "3: Search\n" +
                 "0: Exit"
         )
    fun executeOption(option:Int){
            when (option) {
                1 -> userUI.start();
                2 -> filmUI.start();
                3 -> searchUI.start();
            }
    }

}







