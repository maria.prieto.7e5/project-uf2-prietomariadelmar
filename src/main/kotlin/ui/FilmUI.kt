package ui


import model.Film
import model.FilmITB
import java.util.*

data class FilmUI(val filmITB: FilmITB, val sc: Scanner) {
    /**
     * Inicia el programa
     */
    fun start() {
        do {
            showMenu()
            var option = sc.nextInt()
            executeOption(option)
        } while (option != 0)

    }
    /**
     * Imprime el menu de opciones de pelicula
     */
    fun showMenu() {
        println(
            "Films:\n" +
                    "1: Add film\n" +
                    "2: Show films\n" +
                    "3: Delete films\n" +
                    "4: Watch films\n" +
                    "5: View watched films\n" +
                    "6: Add film to favorites\n" +
                    "7: Show favorites\n" +
                    "8: Show likes per film\n" +
                    "0: Return to main menu"
        )
    }

    /**
     * Ejecuta la opcion del menu escogida por el usuario
     */
    /**
     ** @param option el numero de opcion escogida por el usuario
    */
    fun executeOption(option: Int) {
        when (option) {
            1 ->addFilm(sc.nextLine(),sc.nextLine(),sc.nextLine(),sc.nextLine(), sc.nextInt())
            2 ->showFilm()
            3 -> deleteFilms(sc.nextLine(),sc.nextLine(),sc.nextLine(),sc.nextLine(), sc.nextInt())
            4 -> watchFilm(sc.nextLine())
            5 -> viewWatchedFilm()
            6-> addFavorites(sc.nextLine())
            7-> showFavorites()
            8-> showLikeFilm()
        }
    }
    /**
     * Añade una pelicula a la lista de peliculas
     */
    /**
     ** @param title añade el titulo de la pelicula
     * @param director añade el director de la pelicula
     * @param mainActor añade el actor principal de la pelicula
     * @param genere añade el genero de la pelicula
     * @param lenght añade la duracion de la pelicula
     */


    private fun addFilm(title: String, director: String, mainActor: String, genere: String, lenght: Int) {
        println("Enter a film")
        println("Enter a director")
        println("Enter a main actor")
        println("Enter a genere")
        println("Enter a length")
        val film = Film(title, director, mainActor, genere, lenght)
        filmITB.addFilm(film)
    }

    private fun addFavorites(title: String) {
        println("Enter a favorite film")
        for (film in filmITB.filmList) {
            if (film.title == title) {
                filmITB.favoriteFilm(film)
                break
            }
        }
    }

    private fun showFilm() {
        println(filmITB.filmList)
    }
    /**
     * Elimina una pelicula a la lista de peliculas
     */
    /**
     ** @param title elimina el titulo de la pelicula
     * @param director elimina el director de la pelicula
     * @param mainActor elimina el actor principal de la pelicula
     * @param genere elimina el genero de la pelicula
     * @param lenght elimina la duracion de la pelicula
     */

    private fun deleteFilms(title: String, director: String, mainActor: String, genere: String, lenght: Int) {
        println("Enter a film")
        println("Enter a director")
        println("Enter a main actor")
        println("Enter a genere")
        println("Enter a length")
        val film = Film(title, director, mainActor, genere, lenght)
        filmITB.deleteFilms(film)
    }

        private fun watchFilm(film: String) {
        println("Enter a film you want to watch")
        val title = sc.nextLine()
        for (film in filmITB.filmList) {
            if (film.title == title) {
                filmITB.watchFilm(film)
                break
            }
        }
    }

    private fun viewWatchedFilm() {


    }

    private fun addFilmToFavorites() {
        println("Enter a favorite film")
        val title = sc.nextLine()
        for (film in filmITB.filmList) {
            if (film.title == title) {
                filmITB.favoriteFilm(film)
                break
            }
        }
    }

    fun showFavorites(film: String) {
        val title = sc.nextLine()
        val favoritesFilms = mutableListOf<Film>()
        for (favoriteFilms in filmITB.filmList) {
            if (favoriteFilms.title == title) {
                users.addfavoritesFilms(film)
                return
            }
        }
    }

    fun showLikeFilm() {

    }
}


